import math
import random
import numpy as np
import pandas as pd 
from tabulate import tabulate
from anytree import AnyNode, Node, RenderTree, find_by_attr
from anytree.exporter import DotExporter


# Implements the main DTL algorithm.
# The class should somehow be encoded in the datapoints
# structure. A reasonable approach would be to use
# the feature name "class" to refer to it consistently.
def DTL(data, working_features, classification_col, val, parent=None):
	# recursive_features = working_features.copy()

	winner_attribute = Choose_Attribute(working_features, data)
	# creation of the root
	if not parent:
		winner_node = Node(winner_attribute,display_name=winner_attribute)
		parent = winner_node
	# or the new node
	else:
		winner_node = Node(str(random.sample(range(0,2000),1)[0])+'-'+winner_attribute, parent=parent, display_name=val+'-'+winner_attribute)
		
	# Edge cases:
	# if all of the classifications of data are all the same
	if (data[[classification_col]].nunique().item() == 1):
		classify = list(data[classification_col].unique())	
		Node(str(random.sample(range(0,2000),1)[0])+'-'+classify[0], parent=winner_node, display_name=classify[0])
		return parent 

	# if there are no more features
	if (len(working_features) == 1):
		classes = list(data[classification_col].unique())
		# create the classification node 
		for klass in classes:
			n_class = len(data[data[classification_col] == klass])
			Node(str(random.sample(range(0,2000),1)[0])+'-'+klass, count = n_class, parent=winner_node, display_name=val+'-'+klass)
		return parent 

	# removes the used features
	working_features.remove(winner_attribute)

	# get the answers to split on
	vals = list(data[winner_attribute].unique())
	for val in vals:
		# creates subsets from the answers and calls DTL
		subset = data[data[winner_attribute] == val]
		DTL(subset, working_features.copy(), classification_col, val, winner_node)

	# returns parent, will stack back up to be the root after all the 
		# recursive calls are done
	return parent

# Returns the feature with the greatest information
# gain for the provided datapoints.
def Choose_Attribute(features, datapoints):
	entropy = {} 
	for feat in features:
		feat_subset = datapoints[feat]
		feat_entropy = Compute_Entropy(feat_subset)
		entropy[feat] = feat_entropy
	subset_entropy = min(entropy.values())
	total_entropy = 0
	for col in entropy:
		total_entropy += entropy[col]
	info_gain_feature = Compute_Information(entropy, total_entropy)
	return info_gain_feature 

# Computes the information entropy of datapoints w.r.t
# the class.
# want to pass in the dictionary of entropies to find the total entropy
# then find the 
def Compute_Information(list_of_entropies, total_entropy):
	info_gain = 0
	gains = {}
	for feat in list_of_entropies:
		temp_info_gain = total_entropy - list_of_entropies[feat]
		gains[feat] = temp_info_gain
		if temp_info_gain > info_gain:
			info_gain = temp_info_gain
		else:
			info_gain = info_gain
	for feat in gains:
		if gains[feat] == info_gain:
			info_gain_feat = feat
	return info_gain_feat

# finds the entropy of a pandas table and return a number
def Compute_Entropy(datapoints): 
	# print("Entropy Datapoints", datapoints)
	proportion = datapoints.value_counts() / len(datapoints)
	almost_entropy = proportion * proportion.apply(math.log2)
	entropy = -almost_entropy.sum()
	return entropy


def ID3(file):
	data = pd.read_csv(file).dropna()
	features = list(data)
	class_col = 'classification'
	features.remove(class_col)
	tree = DTL(data, features, class_col, '0',None)
	return tree
	

tree = ID3("congressional_voting_records.csv")
DotExporter(tree, nodeattrfunc=lambda node: 'label="{}"'.format(node.display_name)).to_dotfile("full_data.txt")
DotExporter(tree, nodeattrfunc=lambda node: 'label="{}"'.format(node.display_name)).to_picture("full_data.png")