#A template for the implementation of K-Medoids.
import math #sqrt
import numpy as np
import matplotlib.pyplot as plt

# get and parse data
data = np.genfromtxt('black_tree_data.csv', delimiter = ',')


# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
	#returns a distance between two points
	return sum(np.abs(b - a))
# print(Distance(data[1], data[50]))

# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
# Note: This can be identical to the K-Means function of the same name.
# Copied directly from working implementation of k-means
def assignClusters(D, centers):
	clusters = {}
	for i in range(len(centers)):
		c = centers[i]
		clusters[tuple(c)] = []
	#get distance to each center
	for d in D:
		min_so_far = math.inf
		closest_center = None
		#compare the distances to each other to get the minimum
		for center in centers:
			distance_to_center = Distance(center, d)
			if distance_to_center < min_so_far:
				min_so_far = distance_to_center
				closest_center = center
		#add to clusters
		clusters[tuple(closest_center)].append(d)
	return clusters
# print(assignClusters(data, [data[0], data[70],data[180]]))

def computeCost(D, centers):
	clusters = assignClusters(D, centers)
	cost = 0
	for center in centers:
		for d in clusters[tuple(center)]:
			cost += Distance(center, d)
	return cost


# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Medoids clustering
#  of D.
# Note: very similar to k-means; only difference is findClusterMedoid instead of findClusterMean
def KMedoids(D, k):
	# select k random points from D
	rand_ids = np.random.choice(len(D), size = k, replace = False)
	centers = list(data[rand_ids])
	# Associate each data point to the closest medoid.
	current_cost = computeCost(D, centers)
	last_config_cost = math.inf
	# While the cost of the configuration decreases:
	while current_cost < last_config_cost:
		# print('CURRENT COST', current_cost)
		# print('LAST COST', last_config_cost)
		last_config_cost = current_cost
	# For each medoid m, 
		for i in range(len(centers)):
		# for each non-medoid data point o:
			for j in range(len(D)):
				# Swap m and o, 
				last_swap_cost = current_cost
				old_medoid = centers.pop(i)
				new_medoid = D[j]
				centers.append(new_medoid)
				# recompute the cost (sum of distances of points to their 
				# medoid)
				current_cost = computeCost(D, centers)
				# If the total cost of the configuration 
				# increased in the previous step
				if current_cost > last_swap_cost:
					#undo swap
					centers.pop()
					centers.append(old_medoid)
	clusters = assignClusters(D, centers)
	# for key, data_list in clusters:
	# 	center, cluster = zip(*data_list)
	# 	plt.plot(center, cluster, label = key)
	# plt.show()
	return clusters

print(KMedoids(data, 3))
