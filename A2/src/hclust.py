#A template for the implementation of hclust
import math #sqrt
import numpy as np 
import matplotlib.pyplot as plt
# single linkage


# get and parse data
data = np.genfromtxt('black_tree_data.csv', delimiter = ',')

# finds the closest distance between two clusters
def Distance(a,b):
	# takes two clusters
	cluster_one = a
	cluster_two = b
	smallest_so_far = math.inf
	previous = 0
	# continue until something...
	while smallest_so_far > previous:
		# compares each point in each cluster 
		for i in range(len(cluster_one)):
			for j in range(len(cluster_two)):
			# finds shortest elucidian distance between the two clusters 
				previous = math.sqrt(np.sum((i - j)**2))
				if previous < smallest_so_far:
					smallest_so_far = previous
	# returns the single number (distance between the two points)
	return smallest_so_far

# print("DISTANCE", Distance(data[0], data[15]))

# Accepts two clusters 
# combines them into one cluster
# returns the new cluster
def merge(a,b):
	return a + b

# print("MERGE", merge(data[0], data[15]))

# Accepts a list of data points.
# Returns the pair of points that are closest
# Accepts a list of clusters
# finds the closest two clusters in the list 
# returns it
def findClosestCluster(Clusters):
	start_index = 0
	min_so_far = math.inf
	current_distance = 0
	cluster_one = None
	cluster_two = None
	min_cluster_one = None
	min_cluster_two = None
	# print("MIN SO FAR", min_so_far)
	# print("FINAL MIN", current_dsistance)
	for i in range(len(Clusters)):
		start_index += 1
		for j in range(start_index,len(Clusters)):
			cluster_one = Clusters[i]
			cluster_two = Clusters[j]
			# print('CLUSTER ONE', cluster_one)
			# print('CLUSTER TWO', cluster_two)
			current_distance = Distance(cluster_one, cluster_two)
			if current_distance < min_so_far:
				min_so_far = current_distance
				min_cluster_one = cluster_one 
				min_cluster_two = cluster_two
	return [min_cluster_one, min_cluster_two]

# print("CLOSEST PAIR", findClosestCluster(data))

# Accepts a list of data points.
# Produces a tree structure corresponding to a
# Agglomerative Hierarchal clustering of D.
def HClust(D):
# 	# make each point its own cluster
	clusters = [[d] for d in D]
	all_levels = []
	while len(clusters) > 1:
	# for i in range(len(clusters)):
		# print(len(clusters))
		# findClosestPair and make them one cluster
		closest_cluster = findClosestCluster(clusters)
		# print("CLOSEST PAIR", closest_pair)
		new_cluster = merge(closest_cluster[0], closest_cluster[1])
		clusters.append(new_cluster)
		clusters.remove(closest_cluster[0])
		clusters.remove(closest_cluster[1])
		all_levels += clusters
		# print('LEVEL', i,':', clusters, sep = '')
	return all_levels

print('HCLUST', HClust(data))