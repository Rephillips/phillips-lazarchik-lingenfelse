#A template for the implementation of K-Means.
import math
import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('black_tree_data.csv', delimiter = ',')

# Accepts two data points a and b.
# Returns the euclidian distance
# between a and b.
def euclidianDistance(A,B):
	return math.sqrt(np.sum((A - B)**2))
	

# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
# to center c.
def assignClusters(D, centers):
	clusters = {}
	for i in range(len(centers)):
		c = centers[i]
		clusters[tuple(c)] = []
	#get distance to each center
	for d in D:
		min_so_far = math.inf
		closest_center = None
		#compare the distances to each other to get the minimum
		for center in centers:
			distance_to_center = euclidianDistance(center, d)
			if distance_to_center < min_so_far:
				min_so_far = distance_to_center
				closest_center = center
		#add to clusters
		clusters[tuple(closest_center)].append(d)
	return clusters



# Accepts a list of data points.
# Returns the mean of the points.
def findClusterMean(cluster):
	return np.mean(cluster, axis = 0)


# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Means clustering
#  of D.
def KMeans(D, k):
	#pick k random "center" points
	rand_ids = np.random.choice(len(D), size = k, replace = False)
	working_centers = list(data[rand_ids])
	final_centers = None
	# clusters = assignClusters(D, working_centers)
	while not np.array_equal(final_centers, working_centers):
		clusters = assignClusters(D, working_centers)
		#finding the new centers
		final_centers = np.copy(working_centers)
		working_centers = []
		for that_tuple_that_s_a_key in clusters:
			cluster = clusters[that_tuple_that_s_a_key]
			working_centers.append(findClusterMean(cluster))
	return clusters
	
print(KMeans(data, 3))